FROM node:latest

COPY . .

RUN npm install --production

CMD ["npm", "start"]